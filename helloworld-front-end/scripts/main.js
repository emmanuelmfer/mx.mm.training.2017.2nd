//console.log('Before register window on load callback');
//window.onload = function(){
//	console.log('Window loaded');
//	var n1Node = document.getElementById('n1');
//	var n2Node = document.getElementById('n2');
//	var resultNode = document.getElementById('result');
//	
//	var onAnyNumberChanges = function(){
//		console.log('A number has changed... calculating new result!');
//		var n1Value = parseInt(n1Node.value);
//		var n2Value = parseInt(n2Node.value);
//		
//		var result = n1Value + n2Value;
//		console.log('New value: ' + result);
//		resultNode.value = result;
//	};
//	
//	n1Node.onkeyup = onAnyNumberChanges;
//	n2Node.onkeyup = onAnyNumberChanges;
//};
//console.log('After register window on load callback');
//var sampleUsers = [];
//var emmanuel = new Object();
//emmanuel.fullName = 'Emmanuel Martínez Fernández';
//emmanuel.email = 'lemma@mediomelon.mx';
//sampleUsers.push(emmanuel);
//
//var gilberto = new Object();
//gilberto.fullName = 'Luis Gilberto Sánchez Amador';
//gilberto.email = '84luisgilberto@gmail.com';
//sampleUsers.push(gilberto);

var sampleUsers = [{
		"fullName" : "Emmanuel Martínez Fernández",
		"email" : "lemma@mediomelon.mx"
	}, {
		"fullName" : "Luis Gilberto Sánchez Amador",
		"email" : "84luisgilberto@gmail.com"		
	}
]

var onEditUserClicked = function(){
	console.log('Editing user...');
	
	var $anchorClicked = $(this);
	var $tr = $anchorClicked.parent().parent();
	
	var editingUser = {
		'fullName' : $tr.find('.full-name-cell').text(),		
		'email' : $tr.find('.email-cell').text()		
	};
	
	console.log('User: ' + JSON.stringify(editingUser));
	
	var $editingUserForm = $('#editing-user-form');
	$('#full-name-input').val(editingUser.fullName);
	$('#email-input').val(editingUser.email);
	
	$editingUserForm.show();
	
	
	return false;
};

var loadSampleUsers = function(){
	var $usersTable = $('#users-table');
	var $tbody = $usersTable.find('tbody');
	var $userRowTemplate = $('#user-row-template');
	var template = $userRowTemplate.html();
	for(var i = 0; i < sampleUsers.length; i++){
		var sampleUser = sampleUsers[i];
		console.log('Loading user: ' + JSON.stringify(sampleUser));
		var rowToAppend = template
			.replace('{{fullName}}', sampleUser.fullName)
			.replace('{{email}}', sampleUser.email);
		var $rowToAppend = $(rowToAppend);
		$tbody.append($rowToAppend);
	}
};

$(function(){
	var $n1 = $('#n1');
	var $n2 = $('#n2');
	var $result = $('#result');
	
	loadSampleUsers();
	
	var onAnyNumberChanges = function(){
		var n1Value = parseInt($n1.val());
		var n2Value = parseInt($n2.val());
		var result = n1Value + n2Value;
		$result.val(result);
	};	
	
	$('.edit-user-anchor').click(onEditUserClicked);
	
	$n1.keyup(onAnyNumberChanges);
	$n2.keyup(onAnyNumberChanges);
});